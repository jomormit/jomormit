<?php include_once "CLI_php_coding.php";  ?>
<!DOCTYPE html>
<head>
    <meta charset='UTF-8'>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"
          rel="stylesheet">
    <title>Generate Multiplication Table</title>
</head>

<body>

    <form class="form-horizontal" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Multiplication Table</legend>

            <!-- Text input-->
            <div class="form-group">
                <div class="col-md-4">
                    <input id="largest_value" name="largest_value" type="text" placeholder="Enter value" class="form-control input-md"><br>
                    <span class="help-block">Enter the value to generate multiplication table</span>  
                </div>

            </div>
            <button name="submit" type="submit" value="submit">Submit</button>
        </fieldset>
    </form><br><br>
 </body>
</html>
<?php include_once "Multiply_php_coding.php";  ?>
