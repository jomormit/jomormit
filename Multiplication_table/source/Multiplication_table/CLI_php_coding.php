<?php

declare (strict_types = 1);

// Generate Multiplication table
// setup class autoloading
require __DIR__ . '/../../Application/Autoload/Loader.php';

// add current directory to the path
Application\Autoload\Loader::init(__DIR__ . '/../..');

// get "Multiply generate" class
$mtable = new Application\Web\Multiply();
//if the execution is from command line the default number of value is 20

$isCLI = ( php_sapi_name() == 'cli' );

if ($isCLI) {

    /* Define STDIN in case if it is not already defined by PHP for some reason */
    if (!defined("STDIN")) {
        define("STDIN", fopen('php://stdin', 'r'));
    }

    echo "Hello! Enter number of rows (enter below):\n";
    $row_value = fread(STDIN, 80); // Read up to 80 characters or a newline
   
  
    //then you can use them in a PHP function.
    $resultbrowser = $mtable->generateMultiplicationtableCli(intval($row_value));
    echo $resultbrowser;
    exit();
}
