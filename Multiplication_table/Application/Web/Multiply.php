<?php
// Generate Multiplication table
namespace Application\Web;

/*
 * This app generate multiplication table of an arbitrary size.
 * @Author: Jomon George
 *
 */

class Multiply
{

    /**
     * @param int $numberofrows = number of rows for multiplication table.
     * @author Jomon George
     */
    public function generateMultiplicationtable(int $numberofrows)
    {

        // Loop used to create multiplication table.
        $table = '<table border="1">';

        for ($number = 1; $number <= $numberofrows; $number++) {

            $table .= '</tr>';

            for ($i = 1; $i <= $numberofrows; $i++) {
                if ($number * $i == $i || $i * $number == $number) {
                    $table .= '<td width="60"><b>' . $i * $number . '</b></td>';
                } else {
                    $table .= '<td>' . $i * $number . '</td>';
                }
            }

            $table .= '</tr>';
        }

        $table .= "</table>";

        return $table;
    }


    /**
     *
     * This private function is used to format table specifically for CLI.
     *
     * @param array $tabledata = Contain generated multiplication table based on number of rows.
     * @author Jomon George
     */

    private function tableFormatforCli($tabledata)
    {
        // To find out the longest string
        $columns = [];
        foreach ($tabledata as $rowkey => $row) {
            foreach ($row as $cellkey => $cell) {

                $length = strlen($cell);

                if (empty($columns[$cellkey]) || $columns[$cellkey] < $length) {
                    $columns[$cellkey] = $length;
                }
            }
        }

        // Padding method to output table in cli format
        $table = '';
        foreach ($tabledata as $rowkey => $row) {
            foreach ($row as $cellkey => $cell) {
                $table .= $this->mb_pad($cell, 5);
            }
            $table .= PHP_EOL;
        }
        return $table;
    }
    
    /**
     * @param string $input = number of rows for multiplication table.
     * @param string $padlength = pad length for CLI output.
     * @param $padtype = Determine the space needed.
     * @param string $padstring = padded space.
     * @param int $numberofrows = number of rows for multiplication table.
     * @author Jomon George
     */

    private function mb_pad($input, $padlength, $padstring = ' ', $padtype = STR_PAD_RIGHT)
    {
        $diff = strlen($input) - mb_strlen($this->strip_ansi($input));
        return str_pad($input, $padlength + $diff, $padstring, $padtype);
    }

    
    
    /**
     * @param string $str = string which contain all values.
     * @author Jomon George
     */
    
    private function strip_ansi($str)
    {
         //Implemented strip_ansi method to strip the ansi codes
         $str = preg_replace('/\x1b(\[|\(|\))[;?0-9]*[0-9A-Za-z]/', "", $str);
         $str = preg_replace('/\x1b(\[|\(|\))[;?0-9]*[0-9A-Za-z]/', "", $str);
         $str = preg_replace('/[\x03|\x1a]/', "", $str);
         return $str;
    }

    /**
     *
     * This function is used to generate CLI output of Multiplication table.
     *
     * @param int $numberofrows = number of rows for multiplication table.
     * @author Jomon George
     */
    public function generateMultiplicationtableCli($numberofrows)
    {

        // Loop used to create CLI multiplication table.
        $table = [];

        for ($number = 1, $j = 0; $number <= $numberofrows; $number++, $j++) {

            for ($i = 1, $k = 0; $i <= $numberofrows; $i++, $k++) {

                if ($i == 1 || $number == 1) {

                    $table[$j][$k] = "\033[1m".$i * $number."\033[0m";
                } else {
                    $table[$j][$k] = $i * $number;
                }
            }
        }

        $resulttable = $this->tableFormatforCli($table);

        return $resulttable;


    }
}
