<?php

use PHPUnit\Framework\TestCase;

require '../Web/Multiply.php';

class TestClass extends TestCase
{

    public function testMultiplicationTable()
    {

        $mtable = new Application\Web\Multiply();

        //Testing browser related funcitons
        $this->assertRegExp('!<td>360</td>!', $mtable->generateMultiplicationtable(20));
        
        //Testing CLI functions
        $this->assertRegExp('!360!', $mtable->generateMultiplicationtableCli(20));
        
    }
}
