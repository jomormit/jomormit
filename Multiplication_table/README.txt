This is an application to generate Multiplication table based on user input.

Folders details:

Application : This folder has all details Utilities which support for development. Following are the files used in this application.

Application>Autoload>Loader.php : This php file used to make class available to all pages in development by the way we could keep our class files in a single folder and future updates can be easily done.

Application>Test>TestClass.php : Which is used for test using phpunit to check all methods returning based on assertions of programmer. Code coverage testing is based on this test classes.(I had tested, Everything is fine)

Application>Web>Multiply.php : Which is the core area of application is controller class to give the output of application.


Source : This folder maintain the public webpages of the application.


Source>Multiplication_table>Multiplication_table.php : User interface has written here to run the application.

composer.json : Used to download library files.


HOW TO START APPLICATION.


Requirements:

PHP7 
PHP unittest
Composer

Step 1 : Copy files to the webroot.
Step 2 : Goto file location /source/Multiplication_table/Multiplicaton_table.php


To test my controller:

Do the following steps:

Make sure you have installed PHPunit via composer.

Step 1 : Goto file location Application>Test>TestClass.php
Step 2 : phpunit TestClass.php




HOW TO RUN APPLICATION IN CLI

Make sure you have installed php 7 in terminal. You could verify by typing command "php -v";

Step 1 : Goto file location /source/Multiplication_table/Multiplicaton_table.php;
Step 2 : run command "php Multiplicaton_table.php";

Features: This application can run from both terminal and browser. if you want to render HTML codings in CLI you must installed lynx library


Thank you Jake!
 


    
